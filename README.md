# Django memo
## ターミナルにてスクリプトの実行が無効になっている場合の対応
```
Set-ExecutionPolicy RemoteSigned -Scope Process
```

## コマンド
### プロジェクト作成
```
django-admin startproject [プロジェクト名]
```

### 必要なライブラリのインストール
```
python -m pip install -r requirements.txt
```

### アプリ作成
```
python manage.py startapp アプリ名
```

### 起動準備
1. Djangoにアプリを読み込ませる
2. 現在のモデルに同期させる
3. プロジェクトに問題がないかを確認
4. staticファイルを集積
```
python manage.py makemigrations [アプリ名]
python manage.py migrate
python manage.py check
python manage.py collectstatic
```

### 管理ユーザー作成
```
python manage.py createsuperuser
```

### プロジェクト起動
* デフォルトの場合は「http://127.0.0.1:8000/」に立ち上がる。
```
python manage.py runserver [ipアドレス:ポート番号]
```

### djangoのインストール場所
```
python -c "import django; print(django.__path__)"
```
